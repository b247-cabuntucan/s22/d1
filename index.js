console.log('Hellow Wurld?');

// [SECTION] Array Methods

	// Javascript has built-in functions and methods for arrays. This allow us to manipulate and access array items.

// [SECTION] Mutators Methods
	
	// Mutator methods are functions that 'mutate' or change an array after they're created.
	
	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

		// [SUB-SECTION] push()

		// Adds an element in the end of an array AND returns the array's length
		/*
			Syntax:
				arrayName.push()
		*/

		console.log('Current array:');
		console.log(fruits);

		let fruitsLength = fruits.push('Mango');
		console.log(fruitsLength);
		console.log('Mutated array from push method:');
		console.log(fruits);

		// Adding multiple elements to an array
			fruits.push('Avocado', 'Guava');
			console.log('Mutated array from push method:');
			console.log(fruits);

		// [SUB-SECTION] pop()

		// Removes the last element in an array AND returns the removed element
		/*
			Syntax:
				arrayName.pop()
		*/

		let removedFruit = fruits.pop();
			console.log(removedFruit); // Result will be Guava
			console.log('Mutated array from pop method:');
			console.log(fruits);

		// [SUB-SECTION] unshift()

		// Adds one or more elements at the beginning of an array
		/*
			Syntax:
				arrayName.unshift();
		*/


			fruits.unshift('Lime', 'Banana');
			console.log('Mutated array from unshift method:');
			console.log(fruits);

		// [SUB-SECTION] shift()

		// Removes an element at the beginning of an array AND returns that removed element
		/*
			Syntax:
				arrayName.shift();
		*/

			let anotherFruit = fruits.shift();
				console.log(anotherFruit); // Result will be lime
			console.log('Mutated array from shift method:');
			console.log(fruits);

		// [SUB-SECTION] splice()

		// Simultaneously removes elements from a specified index number and adds elements
		/*
			Syntax:
				arrayName.splice();
		*/

			fruits.splice(1,2, 'Lime', 'Cherry');
			console.log('Mutated array from splice method:');
			console.log(fruits);

		// [SUB-SECTION] sort()

		// Rearranges the array elements in alphanumeric order
		/*
			Syntax:
				arrayName.sort();
		*/

			fruits.sort();
			console.log('Mutated array from splice method:');
			console.log(fruits);

		// [SUB-SECTION] reverse()

		// Reverses the order of array elements
		/*
			Syntax:
				arrayName.reverse()
		*/

			fruits.reverse();
			console.log('Mutated array from reverse method:');
			console.log(fruits);

// [SECTION] Non-Mutators Methods
	
	console.log('Non-Mutators Method');

	// Non-Mutator methods are function that do not modify or change an array after they're created.

	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'FR', 'DE', 'PH'];

	console.log(countries);

	// [SUB-SECTION] indexOf()

	// Returns the index number of the first matching element found in an array
	// If no match was found, the result will be -1.
		/*
			- Syntax
			    arrayName.indexOf(searchValue);
			    arrayName.indexOf(searchValue, fromIndex);
		*/

		let firstIndex = countries.indexOf('PH');
		console.log('Result of indexOf Method: ' + firstIndex);

		let invalidCountry = countries.indexOf('CH');
		console.log('Result of indexOf Method: ' + invalidCountry);

	// [SUB-SECTION] lastIndexOf()

	// Returns the index number of the last matching element found in an array
		/*
			- Syntax
			    arrayName.lastIndexOf(searchValue);
			    arrayName.lastIndexOf(searchValue, fromIndex);
		*/

		// Getting the index number starting from the last element
		let lastIndex = countries.lastIndexOf('PH');
		console.log('Result of lastIndexOf method: ' + lastIndex);

		// Getting the index number starting from a specified index
		let lastIndexStart = countries.lastIndexOf('TH', 4);
		console.log('Result of lastIndexOf method: ' + lastIndexStart);

			let anotherFruits1 = ['apple', 'banana', 'mango', 'kiwi', 'banana'];

			let index0 = anotherFruits1.lastIndexOf('banana');
			console.log(index0);

			index0 = anotherFruits1.lastIndexOf('banana', 3);
			console.log(index0);

	// [SUB-SECTION] slice()

	// Portions/slices elements from an array AND returns a new array
		/*
			- Syntax
			    arrayName.slice(startingIndex);
			    arrayName.slice(startingIndex, endingIndex);
		*/

		let slicedArrayA = countries.slice(2);
		console.log('Result from slice method:');
		console.log(slicedArrayA); // Result will be ['CAN', 'SG', 'TH', ....]
		console.log(countries);

		// Slicing off elements from a specified index to another index
		let slicedArrayB = countries.slice(2, 4);
		console.log('Result from slice method:');
		console.log(slicedArrayB);

		// Slicing off elements starting from the last element of an array
		let slicedArrayC =  countries.slice(-4);
		console.log('Result from slice method:');
		console.log(slicedArrayC);

	// [SUB-SECTION] toString()

	// Returns an array as a string separated by commas
		/*
			- Syntax
			    arrayName.toString();
		*/

			let stringArray = countries.toString();
			console.log('Result from toString Method')
			console.log(stringArray);

	// [SUB-SECTION] concat()

	// Combines two arrays and returns the combined result
		/*
			- Syntax
			    arrayA.concat(arrayB);
			    arrayA.concat(elementA);
		*/

		let tasksArrayA = ['drink html', 'eat javascript'];
		let tasksArrayB = ['inhale css', 'breathe sass'];
		let tasksArrayC = ['get git', 'be node'];

			let tasks = tasksArrayA.concat(tasksArrayB);
			console.log('Result from concat method:');
			console.log(tasks);

			// Combining multiple arrays
			console.log('Result from concat method:');
			let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
			console.log(allTasks);

			// Combining arrays with elements
			let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
			console.log('Result from concat method:');
			console.log(combinedTasks);

	// [SUB-SECTION] join()

	// Returns an array as a string separated by specified separator string
		/*
			- Syntax
			    arrayName.join('separatorString');
		*/

		let users = ['John', 'Jane', 'Joe', 'Robert'];

		console.log(users.join());
		console.log(users.join(''));
		console.log(users.join(' - '))

// [SECTIONS] Iterations Methods
	
	// Iterations methods are loops designed to perform repetitive tasks on arrays


	// [SUB-SECTION] forEach()

		// Similar to a for loop that iterates on each array element
		/*
			Syntax:
				arrayName.forEach(function(indivElement){
					statement
				})
		*/

			allTasks.forEach(function(task1){
				console.log(task1);
			});

			// Using forEach with conditional statements
				let filteredTask = [];

	// [SUB-SECTION] Looping through all Array Items

		// Creating a separate varible to store results of array iteration methods are also good practive to avoid confusion by modifying the original array

			allTasks.forEach(function(task){

				// If the element/string's lenght is greater than 10 characters
				if(task.length > 10){
					
					// Behaviour will be, to add this element in the filteredTasks array
					filteredTask.push(task);
				}
			});

			console.log('Result of filtered tasks:');
			console.log(filteredTask);	

	// [SUB-SECTION] map()

		// Iterates on each element AND returns new array with different values depending on the result of the function's operation
			/*
				- Syntax
				    let/const resultArray = arrayName.map(function(indivElement))
			*/

			let numbers = [1, 2, 3, 4, 5];

			let numbersMap = numbers.map(function(number){
				return number * 3;
			})

			console.log('Original Array:');
			console.log(numbers);
			console.log('Result of map method:');
			console.log(numbersMap)

	// [SUB-SECTION] map() vs. forEach()

			let numberForEach = numbers.forEach(function(number){
				return number * 3
			});

			console.log(numberForEach); // Result will be undefined

			//forEach(), loops over all items in the array as does map(), but forEach() doe not return a new array.

	// [SUB-SECTION] every()

		// Checks if all elements in an array meet the given condition
			/*
				- Syntax
				    let/const resultArray = arrayName.every(function(indivElement) {
				        return expression/condition;
				    })
			*/

			let allValid = numbers.every(function(number){
				return (number < 10);
			});

			console.log('Result of every method:');
			console.log(allValid);

	// [SUB-SECTION] some()

		// Checks if at least one element in the array meets the given condition
			/*
				- Syntax
				    let/const resultArray = arrayName.some(function(indivElement) {
				        return expression/condition;
				    })
			*/

			let someValid = numbers.some(function(number){
				return (number < 2);
			});

			console.log('Result of some method:');
			console.log(someValid);


			// Combining the returned result from the every/some method may be used in other statements to perform consecutive results

			// if(someValid){
			if(allValid){
				console.log('Some numbers in the array are greather then 2');
			};

	// [SUB-SECTION] filter()

		// Returns new array that contains elements which meets the given condition
			/*
				- Syntax
				    let/const resultArray = arrayName.filter(function(indivElement) {
				        return expression/condition;
				    })
			*/

			let filterValid = numbers.filter(function(number){
				return (number < 3);
			});

				console.log('Result of filter method:');
				console.log(filterValid);

				// No elements found
				let nothingFound = numbers.filter(function(number){
					return (number = 0);
				});

				console.log('Result of filter method:')
				console.log(nothingFound);

				// Filtering using foreach
				let filteredNumbers = [];

				numbers.forEach(function(number){
					if(number < 3){
						filteredNumbers.push(number);
					}
				});

				console.log('Result of filter method:')
				console.log(filteredNumbers);

	// [SUB-SECTION] includes()

	// includes() method checks if the arguement passed can be found in the array.
		/*
			- Syntax:
			    arrayName.includes(<argumentToFind>)
		*/

			let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];	

			let productFound1 = products.includes('Mouse');
			console.log(productFound1);

			let productFound2 = products.includes('headset');
			console.log(productFound2);

		/*
			- Methods can be 'chained' using them one after another
			- The result of the first method is used on the second method until all 'chained' methods have been resolved
			- How chaining resolves in our example:
				1. The 'product' element will be converted into all lowercase letters
				2. The resulting lowercased string is used in the 'includes' method
		*/

			let filteredProducts = products.filter(function(product){
				return product.toLowerCase().includes('a','o');
			})

			console.log(filteredProducts);

	// [SUB-SECTION] reduce() 

		// Evaluates elements from left to right and returns/reduces the array into a single value
			/*
				- Syntax
				    let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
				        return expression/operation
				    })
			*/
			// How the 'reduce' method works
				/*
					1. The first/result element in the array is stored in the 'accumulator' parameter
					2. The second/next element in the array is stored in the 'currentValue' parameter
					3. An operation is performed on the two elements
					4. The loop repeats step 1-3 untill all elements have been worked on

				*/

			let iteration = 0;

				console.log(numbers);
				 let numbers1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

			let reducedArray = numbers1.reduce(function(x, y){
				console.warn('current iteration: ' + ++iteration);
				console.log('accumulator: ' + x);
				console.log('currentValue: ' + y);

				return	x + y;
			});

			console.log('Result of reduce method: ' + reducedArray);

			// Reducing String Arrays
				let list = ['Hello', 'Again', 'World', 'Hello1', 'Again1', 'World1', 'Hello2', 'Again2', 'World2'];

				let reducedJoin = list.reduce(function(x,y){
					return x + ' ' + y;
				});

				console.log('Result of reduced method: ' + reducedJoin);


